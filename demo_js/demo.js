// Bài 1: tình tiền lương của nhân viên

// Input: số ngày nhân viên làm= date= 30
// lương 1 ngày const salary=100.000

var date= 30;
const salary= 100000;
var result;
// Steps: lấy date * salary= số tiền lương trong 30 ngày bỏ vào biến result và in ra bằng console
result=date*salary;


// Output: in kết quả ra console
console.log("tiền lương là: "+ result);




// Bài 2: tình giá trị trung bình của 5 số

// Input: khai báo 5 số thực vào 5 biến: a=1, b=2, c=3, d=4, e=5, biến result để lưu kết quả trung bình của 5 biến
var a=1;
var b=2;
var c=3;
var d=4;
var e=5;
var result;
// Steps: cộng 5 giá trị và chia cho 5 để ra trung bình của 5 số, làm tròn trung bình về giá trị gần nhất
result=Math.round((a+b+c+d+e)/5);



// Output: in kết quả ra console
console.log("trung bình của 5 số là:"+ result);



// Bài 3: quy đổi tiền

// Input: khai báo số tiền USD cần đổi vào biến money=12.5, giá trị quy đổi vào biến const exchange, và biến result để lưu kết quả
var money=12.5;
const exchange=23500;
var result;
// Steps: số tiền VND được quy đổi bằng: money * exchange( số tiền nhân với giá trị quy đổi)
result=money*exchange;

// Output: in kết quả ra console
console.log("số tiền quy đổi từ USD sang VND là:"+result);




// Bải 4: Tính diện tích, chu vi hình CN

// Input: khai báo chiều dài và chiều rộng của hình chữ nhận cần tính vào 2 biến là a và b, khai báo 2 biến dientich và chuvi để lưu kết quả của diện tích và chu vi
var a=4; //chiều dài
var b=2; //chiều rộng
var dientich;
var chuvi;


// Steps: diện tích bằng chiều dài * chiều rộng (a*b), chu vi bằng (chiều dài + chiều rộng) *2
dientich= a*b;
chuvi=(a+b)*2;

// Output:
console.log("diện tích là:" +dientich);
console.log("chu vi là: "+chuvi);


// Bài 5: Tình tổng 2 ký số

// Input: khai báo số cần tình tổng 2 ký số vào biến a, khai báo biến result để lưu kết quả, lưu 2 số hàng đơn vị và hàng chục vào 2 biến là donvi và chuc
var a=44;
var donvi;
var chuc;

// Steps: lấy số hàng đơn vị và hàng chục bằng cách lấy số ấy chia cho 10 lấy số dư và lấy kết quả chia
donvi=a %10;
chuc=a/10;
result=Math.round(donvi + chuc);

//Output: in kết quả ra console
console.log("tổng 2 ký số là: "+result);
